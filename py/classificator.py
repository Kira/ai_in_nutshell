import pickle
import sys


def classify(x_test):
    clf_model = load_model()

    result = clf_model.predict(x_test)

    print(result)

    return result;


def load_model():
    pkl_filename = "../clf_model.pkl"

    with open(pkl_filename, 'rb') as file:
        clf_model = pickle.load(file)

    return clf_model;


def load_data():
    file_with_input_data = sys.argv[1]
    x_test = []

    with open(file_with_input_data, 'r') as file:
        i = 0
        for line in file:
            x_test.append(line.strip().split(","))
            i = i + 1

    print(x_test)
    return x_test


def get_results():
    result = classify(load_data())

    save_filename = "../predict.txt"
    with open(save_filename, 'w') as file:
        for line in result:
            file.write(f"{line}\n")


if __name__ == "__main__":
    get_results()
