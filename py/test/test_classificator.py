import unittest

import numpy

from ..classificator import classify


class IrisClassificatorSuccessTest(unittest.TestCase):
    def test_divide(self):
        x_test = [[7.6, 3, 6.6, 2.1],
                  [7.3, 2.9, 6.3, 1.8],
                  [6, 2.9, 4.5, 1.5],
                  [6, 2.7, 5.1, 1.6],
                  [5.8, 4, 1.2, 0.2],
                  [5.4, 3.9, 1.7, 0.4],
                  [6.3, 2.8, 5.1, 1.5],
                  [5, 3, 1.6, 0.2],
                  [4.8, 3.4, 1.6, 0.2],
                  [4.8, 3, 1.4, 0.1],
                  [6.1, 2.9, 4.7, 1.4],
                  [5.7, 2.5, 5, 2],
                  [4.9, 3.1, 1.5, 0.2],
                  [5, 2, 3.5, 1],
                  [4.7, 3.2, 1.3, 0.2],
                  [4.8, 3.4, 1.9, 0.2],
                  [6.3, 2.7, 4.9, 1.8],
                  [5, 3.2, 1.2, 0.2],
                  [6.7, 3.3, 5.7, 2.1],
                  [6.2, 2.2, 4.5, 1.5]]

        expected_result = [2, 2, 1, 1, 0, 0, 2, 0, 0, 0, 1, 2, 0, 1, 0, 0, 2, 0, 2, 1]
        result = classify(x_test)

        self.assertTrue(numpy.alltrue(expected_result == result), "results are different")


if __name__ == "__main__":
    unittest.main()
